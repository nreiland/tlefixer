// residual.cpp

#include"fixerlib.h"
#include"./thalassa/thalassa.h"
#include <cmath>
#include <iostream>
#include <iomanip>

using namespace fixer;

// constructor
residual::residual() {

    // default name
    name = "uninitialized";

    // epoch
    mjd = 0;

    // allocate orbital elements
    for (int i = 0; i < 6; i++) {
        errCoe.push_back(0);
    }

    // allocate Cartesian states
    for (int i = 0; i < 3; i++) {
        errRgcrf.push_back(0);
        errVgcrf.push_back(0);
    }

};

// function to retrieve name
std::string residual::getName() {
    return name;
};

// function to set name
void residual::setName(std::string name) {
    this->name = name;
};

// function to compute residual from observations
void residual::compute(pred predState, truth truthState) {

    // locals
    double tol;
    double mjdErr;
    std::vector<double> trueRgcrf;
    std::vector<double> trueVgcrf;
    std::vector<double> predRgcrf;
    std::vector<double> predVgcrf;
    std::vector<double> predCoe;
    std::vector<double> trueCoe;

    // check that epochs are the same
    mjdErr = truthState.getMjd() - predState.getMjd();
    mjdErr = abs(mjdErr);

    // check tolerance
    tol = 1.0/100.0/(60.0*60.0*24.0);
    if (mjdErr > tol) {
        std::cout << "warning, epoch error between predicted and ";
        std::cout << "truth states is larger than specified tolerance:\n";
        std::cout << "truth Epoch = MJD " << std::setprecision(10) << truthState.getMjd() << "\n";
        std::cout << "pred Epoch  = MJD " << std::setprecision(10) << predState.getMjd() << "\n";
        std::cout << "tolerance   = " << std::setprecision(10) << tol << "\n";
    }

    // save pred epoch
    mjd = predState.getMjd();

    // compute Keplerian orbital elements error
    trueCoe = truthState.getCoe();
    predCoe = predState.getCoe();
    for (int i = 0; i < 6; i++) {
        errCoe[i] = abs(trueCoe[i] - predCoe[i]);
    }

    // allocate Cartesian position vectors
    trueRgcrf = truthState.getRgcrf();
    predRgcrf = predState.getRgcrf();
    for (int i = 0; i < 3; i++) {
        errRgcrf[i] = abs(trueRgcrf[i] - predRgcrf[i]);
    }

    // allocate Cartesian velocity vectors
    trueVgcrf = truthState.getVgcrf();
    predVgcrf = predState.getVgcrf();
    for (int i = 0; i < 3; i++) {
        errVgcrf[i] = abs(trueVgcrf[i] - predVgcrf[i]);
    }

};

// function to print object information
void residual::print() {

    std::cout << "\nConsidering residual state ";
    std::cout << name << ' ';

    std::cout << "\nThe Modified Julian is ";
    std::cout << mjd << "\n";

    std::cout << "\nThe residuals in the Keplerian orbital elements are : ";
    for (int i=0; i < 6; i++) {
        std::cout << errCoe[i] << ' ';
    }

    std::cout << "\nThe residuals in the GCRF position vector elements are : ";
    for (int i=0; i < 3; i++) {
        std::cout << errRgcrf[i] << ' ';
    }

    std::cout << "\nThe residuals in the GCRF velocity vector elements are : ";
    for (int i=0; i < 3; i++) {
        std::cout << errVgcrf[i] << ' ';
    }

    std::cout << "\n";

};

// function to get MJD
double residual::getMjd() {
    return mjd;
}

// function to get orbital elements
std::vector<double> residual::getErrCoe() {
    return errCoe;
};

// function to get position vector
std::vector<double> residual::getErrRgcrf() {
    return errRgcrf;
};

// function to get velocity vector
std::vector<double> residual::getErrVgcrf() {
    return errVgcrf;
};