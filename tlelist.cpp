// tlelist.cpp

#include<cstdlib>  
#include"fixerlib.h"
#include<string>
#include<iostream>
#include<fstream>

using namespace fixer;

tlelist::tlelist(const char *fin, eop eopData, std::string namein) {

    // head node
    head = NULL;
    curr = NULL;
    name = namein;

    std::cout << fin << "\n";

    // open input file
    std::ifstream input;
    input.open(fin, std::ifstream::in);
    tlelist::populate(input, eopData);
};

void tlelist::addNode(std::string name, char line1[130], char line2[130]) {

    // define new TLE node
    nodePtr n = new node;
    n->next = NULL;
    n->obj.initialize(name, line1, line2);

    if (head != NULL) {
        curr = head;
        while (curr->next != NULL) {
            curr = curr->next;
        }
        curr->next = n;
        curr->next->idx = curr->idx + 1;
        curr = curr->next;
    }
    else {
        head = n;
        curr = head;
        curr->idx = 0;
    }

};

void tlelist::populate(std::ifstream& ifs, eop eopData) {

    // local variables
    char line1[130];
    char line2[130];
    char check1[1], check2[2];
    const char *format1, *format2;

    // end of file reached
    if (ifs.eof()) {
        ifs.close();
        return;
    }

    // end of file not reached
    else {
        
        // read lines
        ifs.getline(line1,130);
        if (ifs.eof()) {
            ifs.close();
            return;
        }
        ifs.getline(line2,130);
        if (ifs.eof()) {
            ifs.close();
            return;
        }

        // construct tle name
        std::string s(line1);
        std::string lname(s, 2, 5); //starting at 2, with a length of 5

        // add node
        addNode(lname, line1, line2);

        // add index to name
        curr->obj.setName(name + "_" + std::to_string(curr->idx));

        // transform satellite state into PEF from TEME
        curr->obj.teme2pef(eopData);

        // transform satellite state into ITRF from PEF
        curr->obj.pef2itrf(eopData);

        // transform satellite state into GCRF from PEF
        curr->obj.itrf2gcrf(eopData);

        // transform gcrf to coe
        curr->obj.gcrf2coe();

        // recurse
        tlelist::populate(ifs, eopData);

    }

};

// function to print tle list
void tlelist::printList() {

    if (head != NULL) {
        curr = head;
        while (curr != NULL) {
            curr->obj.print();
            curr = curr->next;
        }
        std::cout << "[EoF reached]\n";
        
    }
    else {
        std::cout << "list is empty\n";
    }
   
 };

// go to head
void tlelist::goToHead() {
    curr = head;
};

// function to go to next node
void tlelist::advance() {
    curr = curr->next;
}

// get current node
tle* tlelist::getCurr() {
    if (curr == NULL) {
        return NULL;
    }
    else {
        return &curr->obj;
    }
};