#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include "./thalassa/thalassa.h"
#include "fixerlib.h"

using namespace fixer;

int main()
{

    // input files
    char fin[]    = "in/lageos1_TLE.txt";
    char feop[]   = "in/current10_FINALS.DATA_IAU2000_V2013_0110.txt";
    char fsp3[]   = "in/sp3s/longarc/fourweek/bkg.orb.lageos1.fourweek.sp3";
    char ftrue[]  = "out/trueStates.txt";
    char fpred[]  = "out/predStates.txt";
    char fres[]   = "out/resStates.txt";
    char l1Dir[]  = "/Users/nreiland/Documents/lageos2";
    char tleDir[] = "tleDataL2";
    char resDir[] = "residualsDataL2";

    // set info
    sp3FileList sp3Set;
    sp3Set.setNoradId(22195);
    sp3Set.setTleDir(tleDir);
    // sp3Set.setSpaceTrackInfo("nreiland@email.arizona.edu", "notsurewhyineedapassword");
    sp3Set.setSpaceTrackInfo("woodywu@email.arizona.edu", "Deskjet3848woody");

    // decompress
    std::cout << "decompress\n";
    sp3Set.uncompress(l1Dir);

    // set parameters
    std::cout << "set parameters\n";
    sp3Set.setResidualDir(resDir);
    sp3Set.setEopFile(feop);

    // populate set
    std::cout << "populate\n";
    sp3Set.populate(l1Dir);

    // debug
    // sp3Set.printList();

    // compute residuals
    std::cout << "generating residuals\n";
    sp3Set.genResiduals();

    

}