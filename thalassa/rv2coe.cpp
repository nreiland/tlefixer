//rv2coe.cpp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "thalassa.h"

// function declarations
extern "C" void cart2coe4c_(double R[], double V[], double COE[], double *GM);

/*main function*/
void rv2coe(double r[], double v[], double coe[], double GM) {

    // call fortran function
    cart2coe4c_(r, v, coe, &GM);
}