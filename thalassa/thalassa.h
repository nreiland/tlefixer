// thalassa input type
typedef struct {
   double MJD0;
   double COE0[6]; 
   double tspan; 
   double tstep; 
   int insgrav; 
   int isun; 
   int imoon; 
    int idrag; 
    int iF107; 
    int iSRP; 
    int iephem; 
    int gdeg; 
    int gord; 
    double tol; 
    int imcoll;
    int eqs; 
    double SCMass; 
    double ADrag; 
    double ASRP; 
    double CD; 
    double CR; 
    int npts; 
    double *cart;
    double *orbs; 
    int tag;
    int exitcode;  
} thalassaInput;

// function to call thalassa
void thalassa(thalassaInput *in);

// C wrapper funciton
void rv2coe(double r[], double v[], double coe[], double GM);

// physical constants used in thalassa
#define MU 3.986004414498200E+05
#define r2d (180.0/M_PI)
