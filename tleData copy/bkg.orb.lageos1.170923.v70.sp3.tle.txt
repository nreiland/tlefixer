<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Space-Track.Org</title>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Space-Track.Org">
    <meta name="keywords"    content="Space, TLE, orbit, satellites, space objects">
    <meta name="author"      content="SAIC admin@space-track.org">
    <meta name="googlebot"   content="noindex">
    <meta name="apple-mobile-web-app-title" content="Space-Track">
    <meta name="application-name" content="Space-Track">
    <meta name="theme-color" content="#ffffff">

    <link rel="apple-touch-icon" sizes="180x180" href="https://www.space-track.org/assets/ico/production/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://www.space-track.org/assets/ico/production/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://www.space-track.org/assets/ico/production/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://www.space-track.org/assets/ico/production/android-chrome-192x192.png">
    <link rel="manifest" href="https://www.space-track.org/assets/ico/production/manifest.json">
    <link rel="mask-icon" href="https://www.space-track.org/assets/ico/production/safari-pinned-tab.svg" color="#2b5797">
    <link rel="shortcut icon" href="https://www.space-track.org/assets/ico/production/favicon.ico?v=m2l0MBPYLl" type="image/ico">

    <script src="https://www.space-track.org/assets/js/jquery-2.1.4.min.js"></script>
    <script src="https://www.space-track.org/assets/js/bootstrap.min.js"></script>
    <script src="https://www.space-track.org/assets/js/st-custom.js"></script>
    
    <link href="https://www.space-track.org/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.space-track.org/assets/css/theme/st-slate/bootstrap-theme.min.css" rel="stylesheet">
    <link href="https://www.space-track.org/assets/css/st-custom.css" rel="stylesheet">
    <link href="https://www.space-track.org/assets/css/theme/st-slate/st-custom.css" rel="stylesheet"></head>
<body>
<div id="container" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <h2>Uh Oh. Something Has Gone Horribly, Horribly Wrong!</h2>
                <p>You've violated your query rate limit.  Please refer to our Acceptable Use guidelines for further information on how to avoid this message in the future. ( https://www.space-track.org/documentation ) ( https://www.space-track.org/documentation )</p>                <p>If you are coming back to the site after a while, your session may have expired. If so please <a href="/auth/login">log in</a> and try again.</p>
                <p class="text-info">Go <a href="#" onclick="window.history.back();window.location.href='/'; return false;">back</a> or <a href="/">Return to Home Page</a></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 centered">
            <img src="https://www.space-track.org/assets/img/error.jpg" alt="Meteor Hitting Earth" style="width:100%;max-width:800px;" />
        </div>
    </div>
        <div class="push"></div>
        <footer class="footer navbar-fixed-bottom">
                        <p>
                <a href="https://www.facebook.com/SpaceTrack" target="_blank"><img src="https://www.space-track.org/assets/ico/facebooklogo.png" class="social-media-icon" alt="Facebook"></a>
                <a href="https://twitter.com/spacetrackorg" target="_blank"><img src="https://www.space-track.org/assets/ico/twitterlogo.png" class="social-media-icon" alt="Twitter"></a>
                &nbsp;&nbsp;
                Developed by <a href="http://www.saic.com" target='_blank'>SAIC</a> under contract to CFSCC CJ3/6. <a href="https://www.space-track.org/documentation#/contact">Contact Us</a>
            </p>
        </footer>
    </div><!-- /container -->
</body>
</html>
