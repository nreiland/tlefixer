// eop.cpp

#include<cstdlib>  
#include"fixerlib.h"
#include<string>
#include<iostream>
#include<fstream>
#include<stdio.h>

using namespace fixer;

// indexing function
int eop::index (int mjdEpoch) {

    // local varialbes
    int imjdDiff;
    
    // get eop data index
    int imjdCurr = mjdEpoch;
    int imjd0 = mjd.front();
    int imjdf = mjd.back();

    // check for unprovided data
    if (imjdCurr < imjd0) {
        std::cout << "Requested date, " << imjdCurr << ' ';
        std::cout << ", is before start of Earth Orientation Data, ";
        std::cout << imjd0 << ' ' << ".\n";
        std::exit(0);
        return 0;
    }
    else if (imjdCurr > imjdf) {
        std::cout << "Requested date, " << imjdCurr << ' ';
        std::cout << ", is after end of Earth Orientation Data, ";
        std::cout << imjdf << ' ' << ".\n";
        std::exit(1);
        return 0;
    }

    // get index of requested mjd
    imjdDiff = imjdCurr - imjd0;
    
    // return index
    return imjdDiff;

};

// initialization function
void eop::initialize(std::ifstream& ifs) {

    char line[200];

    // end of file reached
    if (ifs.eof()) {
        ifs.close();
        return;
    }

    // end of file not reached
    else {

        // locals
        double mjdi, pmxi, pmyi, UT1_UTCi, LODi, dx00i, dy00i;
        char mjds[10], pmxs[15], pmys[15], UT1_UTCs[17], LODs[11];
        char dx00s[12], dy00s[12];
        
        // read lines
        ifs.getline(line, 200);

        // sscanf
        const char *format;
        format = "%*7c%10s%*3c%15s%*10c%15s%*12c%17s%*11c%11s%*11c%12s%*10c%12s%*67c";
        sscanf(line, format, mjds, pmxs, pmys, UT1_UTCs, LODs, dx00s, dy00s);

        // convert to double precision number
        mjdi = std::atof(mjds); 
        pmxi = std::atof(pmxs);
        pmyi = std::atof(pmys); 
        UT1_UTCi = std::atof(UT1_UTCs);
        LODi = std::atof(LODs);
        dx00i = std::atof(dx00s);
        dy00i = std::atof(dy00s);

        // add new values to vectors
        mjd.push_back(mjdi);
        pmx.push_back(pmxi);
        pmy.push_back(pmyi);
        UT1_UTC.push_back(UT1_UTCi);
        LOD.push_back(LODi);
        dx00.push_back(dx00i);
        dy00.push_back(dy00i);

        // recurse
        eop::initialize(ifs);

    }

};

// constructor
eop::eop(const char *fin) {

    std::ifstream eopInput;
    eopInput.open(fin, std::ifstream::in);
    eop::initialize(eopInput);
    eopInput.close();

    
};

// getting functions
double eop::getMjd(int idx) {
    return mjd[idx];
};

double eop::getPmx(int idx) {
    return pmx[idx];
};

double eop::getPmy(int idx) {
    return pmy[idx];
};

double eop::getUt1_Utc(int idx) {
    return UT1_UTC[idx];
};

double eop::getLod(int idx) {
    return LOD[idx];
};

double eop::getDx00(int idx) {
    return dx00[idx];
};

double eop::getDy00(int idx) {
    return dy00[idx];
};

// print list
void eop::print() {

    std::cout << "\nThe EOP parameters are : ";
    for (int i=0; i < mjd.size(); i++) {
        std::cout << mjd[i] << ", " << pmx[i] << ", " << pmy[i] << ", " << UT1_UTC[i];
        std::cout << ", " << LOD[i] << ", " << dx00[i] << ", " << dy00[i] << "\n";
    }
}