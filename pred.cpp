// pred.cpp

#include"fixerlib.h"
#include"./thalassa/thalassa.h"

using namespace fixer;

// constructor
pred::pred() {

    // default name
    name = "uninitialized";

    // epoch
    mjd = 0;

    // allocate orbital elements
    for (int i = 0; i < 6; i++) {
         coe.push_back(0);
    }

    // allocate Cartesian states
    for (int i = 0; i < 3; i++) {
        rgcrf.push_back(0);
        vgcrf.push_back(0);
    }

};

// function to retrieve name
std::string pred::getName() {
    return name;
};

// function to set name
void pred::setName(std::string name) {
    this->name = name;
};

// function to initialize truth state from TLE
void pred::genFromTle(tle *tleState) {

    // set name
    name = tleState->getName();

    // set epoch
    mjd = tleState->getMjd0();

    // set orbital elements
    coe = tleState->getCoe0();

    // set position vector
    rgcrf = tleState->getGcrf0();

    // set velocity vector
    vgcrf = tleState->getVgcrf0();  

}

// function to get mjd
double pred::getMjd() {
    return mjd;
};

// function to get orbital elements
std::vector<double> pred::getCoe() {
    return coe;
};

// function to get position vector
std::vector<double> pred::getRgcrf() {
    return rgcrf;
};

// function to get velocity vector
std::vector<double> pred::getVgcrf() {
    return vgcrf;
};

// function to print object information
void pred::print() {

    std::cout << "\nConsidering pred state ";
    std::cout << name << ' ';

    std::cout << "\nThe Modified Julian is ";
    std::cout << mjd << "\n";

    std::cout << "\nThe Keplerian orbital elements are : ";
    for (int i=0; i < 6; i++) {
        std::cout << coe[i] << ' ';
    }

    std::cout << "\nThe GCRF position vector elements are : ";
    for (int i=0; i < rgcrf.size(); i++) {
        std::cout << rgcrf[i] << ' ';
    }

    std::cout << "\nThe GCRF velocity vector elements are : ";
    for (int i=0; i < vgcrf.size(); i++) {
        std::cout << vgcrf[i] << ' ';
    }

    std::cout << "\n";

};