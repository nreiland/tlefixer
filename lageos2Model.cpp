//lageos1Model.cpp

#include "fixerlib.h"

using namespace fixer;

// function definition
physModel fixer::lageos2Model() {

    // locals
    physModel model;

    // define model
    model.tstep   = 1;
    model.insgrav = 1;
    model.isun    = 1;
    model.imoon   = 1;
    model.idrag   = 4;
    model.iF107   = 1;
    model.iSRP    = 2;
    model.iephem  = 1;
    model.gdeg    = 20;
    model.gord    = 20;
    model.tol     = 1.00000E-14;
    model.imcoll  = 0;
    model.eqs     = 2;
    model.SCMass  = 406.95;
    model.ADrag   = 0.2827433388230814;
    model.ASRP    = 0.2827433388230814;
    model.CD      = 2.1;
    model.CR      = 1.125; 

    return model;
}

    