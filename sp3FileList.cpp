//residualList.cpp
#include<cstdlib>  
#include"fixerlib.h"
#include<string>
#include<iostream>
#include<fstream>
#include<dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "SOFA/sofa.h"
#include <string>

using namespace fixer;

// function to check file type
int is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
};

// function to check if file exists
int fileExists (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

// constructor
sp3FileList::sp3FileList() {

    // head node
    head = NULL;
    curr = NULL;
    name = "uninitialized";
    idx = 0;

};

// function to set norad id
void sp3FileList::setNoradId(int nID) {
    this->nID = nID;
}

// function to set tle data output dir
void sp3FileList::setTleDir(std::string tleDir) {
    this->tleDir = tleDir;
}

// function to set spacetrack username and password
void sp3FileList::setSpaceTrackInfo(std::string username, std::string password) {
    STusername = username;
    STpassword = password;
}

// funciton to set residuals directory
void sp3FileList::setResidualDir(std::string resDir) {
    this->resDir = resDir;
}

// funciton to set eop file
void sp3FileList::setEopFile(std::string feop) {
    this->feop = feop;
}

// function to return mjd of sp3 lines
double readMjd(const char *line1, const char *line2, const char *line3) {

    // string locals
    char iyears[50], imonths[50], idays[50], ihours[50], imins[50], secs[50];
    char vids[50], vids2[50], xcords[50], ycords[50], zcords[50], clocks[50];
    char xdots[50], ydots[50], zdots[50], dclocks[50];

    // number locals
    int iyear, imonth, iday, ihour, imin;
    double sec, xcord, ycord, zcord, clock;
    double xdot, ydot, zdot, dclock;
    double djm0, djm, time, jd0, mjd0;

    // sscanf
    const char *format1, *format2, *format3;
    
    // epoch format
    format1 = "%*3c%4s%*c%2s%*c%2s%*c%2s%*c%2s%*c%11s";

    // position format
    format2 = "%*c%3s%14s%14s%14s%14s";

    // velocity format
    format3 = "%*4c%14s%14s%14s%14s";

    // scan
    sscanf(line1, format1, iyears, imonths, idays, ihours, imins, secs);
    sscanf(line2, format2, vids, xcords, ycords, zcords, clocks);
    sscanf(line3, format3, xdots, ydots, zdots, dclocks);

    // covert integers
    iyear  = std::stoi(iyears);
    imonth = std::stoi(imonths);
    iday   = std::stoi(idays);
    ihour  = std::stoi(ihours);
    imin   = std::stoi(imins);

    // convert doubles
    sec    = std::atof(secs); 
    xcord  = std::atof(xcords);
    ycord  = std::atof(ycords); 
    zcord  = std::atof(zcords);
    clock  = std::atof(clocks);
    xdot   = std::atof(xdots);
    ydot   = std::atof(ydots);
    zdot   = std::atof(zdots);
    dclock = std::atof(dclocks);

    // convert Calendar date to mjd and jd
    iauCal2jd(iyear, imonth, iday, &djm0, &djm);
    time = (60.0*(double)(60*ihour + imin) + sec)/DAYSEC;
    jd0  = djm0 + djm + time;
    mjd0 = jd0 - djm0;

    return mjd0;
};

// date range reader
void sp3FileList::dateRanges(const char *sp3Fname, double *mjd0, double *mjdf) {

    // local variables
    char line[200];
    char sval[10];
    char lval[10];
    char line1[200];
    char line2[200];
    char line3[200];
    char dummy[200];
    const char *format, *lformat;
    int n, i;

    // open input file
    std::ifstream input;
    input.open(sp3Fname, std::ifstream::in);

    // skip lines of input to bypass header of sp3 
    format = "%1s%*59c\n";
    input.getline(line, 200);
    sscanf(line, format, sval);

    // count lines
    n = 0;
    while (*sval != '*') {
        n = n + 1;
        input.getline(line, 200);
        sscanf(line, format, sval);
    }
    input.close();

    // reopen and advance to first state
    input.open(sp3Fname, std::ifstream::in);
    for (i = 0; i < n; i++) {
        input.getline(line, 200);
    }   

    // read first three lines
    input.getline(line1,200);
    input.getline(line2,200);
    input.getline(line3,200);
    input.close();

    // get initial mjd
    *mjd0 = readMjd(line1, line2, line3);

    // count lines to end of file
    lformat = "%3s%*59c\n";
    n = 0;
    input.open(sp3Fname, std::ifstream::in);
    while (strcmp(lval,"EOF") != 0) {
        n = n + 1;
        input.getline(line, 200);
        sscanf(line, lformat, lval);
        // std::cout << lval << "\n";
    }
    input.close();

    // reopen and advance to final state
    input.open(sp3Fname, std::ifstream::in);
    for (i = 0; i < n - 4; i++) {
        input.getline(line, 200);
    }

    // read last three lines
    input.getline(line1,200);
    input.getline(line2,200);
    input.getline(line3,200);

    // get last mjd
    *mjdf = readMjd(line1, line2, line3);

    // close file
    input.close();
}

// function to download tle corresponding to date ranges
void sp3FileList::downloadTles(int mjd0, int mjdf, std::string tleFname) {

    // check if file has already been downloaded
    if (fileExists(tleFname) == 0) {  

        // debug
        // std::cout << fileExists(tleFname) << "\n";
        std::cout << "downloading: " << tleFname << "\n";    

        // define system call
        std::string space = " ";
        std::string command = "python3 fetchTle.py" + space;
        command = command + std::to_string(nID) + space + std::to_string(mjd0) + space;
        command = command + std::to_string(mjdf) + space + tleFname + space;
        command = command + STusername+ space + STpassword;

        // call python functions through system
        // std::cout << command << "\n";
        system(command.data());
    }
    else {
        std::cout << tleFname << " has already been downloaded\n";
    }

}

// function to add node
void sp3FileList::addNode(const char *sp3Fname, const char *tleFname) {
    
    // define new residual node
    nodePtr n = new node;
    n->next = NULL;
    n->sp3Fname = sp3Fname;
    n->tleFname = tleFname;
    dateRanges(sp3Fname, &n->mjd0, &n->mjdf);
    downloadTles(n->mjd0, n->mjdf, n->tleFname);

    if (head != NULL) {
        curr = head;
        while (curr->next != NULL) {
            curr = curr->next;
        }
        curr->next = n;
        curr = curr->next;
    }
    else {
        head = n;
        curr = head;
    }

};

// function to populate list
void sp3FileList::populate(const char *dirName) {

    // locals
    char sval[3], check1[1], check2[2];
    const char *format, *format1, *format2;
    char *subDir;
    char *parent;
    char *command1, *command2;
    std::string tleName;

    // copy parent directory name
    parent = (char*)malloc( strlen(dirName) + 5*sizeof(char) ); 
    strcpy(parent, dirName);

    // go to head
    curr = head;

    // load sp3 files in directory
    DIR *dir;
    struct dirent *ent;

    // open directory
    dir = opendir (dirName);

    if ( dir != NULL) {

        // print all the files and directories within directory
        while ((ent = readdir (dir)) != NULL) {

            // define sub directory name
            subDir = (char*)malloc( strlen(parent) + strlen(ent->d_name) + 5*sizeof(char) ); 
            strcpy(subDir, parent);
            strcat(subDir, "/");
            strcat(subDir, ent->d_name);

            // read file type
            format = "%3s%*32c\n";
            sscanf(ent->d_name, format, sval);

            // check if file is bkg
            if ( strncmp(sval, "bkg", 3) == 0 ) {

                // load if uncompressed
                if (ent->d_name[strlen(ent->d_name) - 1] != 'Z') {

                    // define tle name
                    tleName = ent->d_name;
                    tleName = tleName + ".tle.txt";
                    tleName = tleDir + "/" + tleName;
                    
                    // add node
                    addNode(subDir, tleName.data());

                } 
            }
            else {

                
                // printf("checking: %s\n", subDir);

                // load checks
                format1 = "%1s\n";
                format2 = "%2s\n";
                sscanf(ent->d_name, format1, check1);
                sscanf(ent->d_name, format2, check2);
                // printf("checks are: %s and %s\n",check1,check2);

                // check if it's a regular file
                if ( strncmp(check1, ".", 1) != 0 && strncmp(check2, "..", 2) != 0 && !is_regular_file(subDir)) {

                    // recurse
                    // printf(" recursing on %s\n",subDir);
                    sp3FileList::populate(subDir);
                    
                }
                
            }

            // free name
            free(subDir);

            // debug
            // idx = idx + 1;
            // std::cout << "idx = " << idx << "\n";
        }

        // close directory
        closedir (dir);
    }

    // free allocs
    free(parent);

};

// function to print list to terminal
void sp3FileList::printList() {

    if (head != NULL) {
        curr = head;
        while (curr != NULL) {
            std::cout << curr->sp3Fname << "\n";
            std::cout << curr->tleFname << "\n";
            std::cout << curr->mjd0 << "\n";
            std::cout << curr->mjdf << "\n";
            curr = curr->next;
        }        
    }
    else {
        std::cout << "list is empty\n";
    }
        
};

// function to recurse and uncompress bkg sourced sp3 files
void sp3FileList::uncompress(const char *dirName) {

    // locals
    char sval[3], check1[1], check2[2];
    const char *format, *format1, *format2;
    char *subDir;
    char *parent;
    char *command1, *command2;

    // copy parent directory name
    parent = (char*)malloc( strlen(dirName) + 5*sizeof(char) ); 
    strcpy(parent, dirName);

    // go to head
    curr = head;

    // load sp3 files in directory
    DIR *dir;
    struct dirent *ent;

    // open directory
    dir = opendir (dirName);

    if ( dir != NULL) {

        // print all the files and directories within directory
        while ((ent = readdir (dir)) != NULL) {

            // define sub directory name
            subDir = (char*)malloc( strlen(parent) + strlen(ent->d_name) + 5*sizeof(char) ); 
            strcpy(subDir, parent);
            strcat(subDir, "/");
            strcat(subDir, ent->d_name);

            // read file type
            format = "%3s%*32c\n";
            sscanf(ent->d_name, format, sval);

            // check if file is bkg
            if ( strncmp(sval, "bkg", 3) == 0 ) {

                // uncompress if needed
                if (ent->d_name[strlen(ent->d_name) - 1] == 'Z') {
                    // printf("uncompressing : %s\n", subDir);

                    // define command 1
                    command1 = (char*)malloc( strlen(subDir) + 50*sizeof(char) ); 
                    strcpy(command1, "uncompress -f ");
                    strcat(command1, subDir);

                    // define command 2
                    command2 = (char*)malloc( strlen(subDir) + 50*sizeof(char) ); 
                    strcpy(command2, "rm -rf ");
                    strcat(command2, subDir);

                    // uncompress
                    system(command1);
                    // printf("%s\n",command1);

                    // delete
                    // system(command2);
                    // printf("%s\n",command2);

                    // free commands
                    free(command1);
                    free(command2);

                }
                
            }
            else {

                
                // printf("checking: %s\n", subDir);

                // load checks
                format1 = "%1s\n";
                format2 = "%2s\n";
                sscanf(ent->d_name, format1, check1);
                sscanf(ent->d_name, format2, check2);
                // printf("checks are: %s and %s\n",check1,check2);

                // check if it's a regular file
                if ( strncmp(check1, ".", 1) != 0 && strncmp(check2, "..", 2) != 0 && !is_regular_file(subDir)) {

                    // recurse
                    sp3FileList::uncompress(subDir);
                    // printf(" recursing on %s\n",subDir);
                }
                
            }

            // free name
            free(subDir);

            
        }

        // close directory
        closedir (dir);
    }

    // free allocs
    free(parent);

};

// helper function to perform loop
void sp3FileList::performLoop(int idx, eop eopData) {

    // locals
    std::string currDir;
    std::string ftrue;
    std::string fpred;
    std::string fres;

    // generate list of tles
    tlelist lageos1tles(curr->tleFname.data(), eopData, "lageos1");

    // print list of tles
    // lageos1tles.printList();

    // load sp3 files
    sp3list lageos1slr(curr->sp3Fname.data(), &eopData, "lageos1", 2, dms2kms);

    // print list of sp3s
    // lageos1slr.printList();

    // define physical model
    physModel model = lageos2Model();

    // compute residuals
    residualList residuals;
    residuals.populate(&lageos1tles, &lageos1slr, model);

    // print residuals
    // residuals.printList();

    // make output directory
    currDir = resDir;
    currDir = resDir + "/" + std::to_string(idx);
    mkdir(currDir.data(), 0777);

    // define output files
    ftrue = currDir + "/trueStates.txt";
    fpred = currDir + "/predStates.txt";
    fres  = currDir + "/resStates.txt";

    // write to output files
    residuals.writeToFile(ftrue.data(), fpred.data(), fres.data());

};

// function to generate residuals for list
void sp3FileList::genResiduals() {

    // local variables
    int idx;

    // go to heads of list
    curr = head;

    // load eop data
    eop eopData(feop.data());

    // set initial index
    idx = 1;

    // iterate through tle list
    while (curr != NULL) {

        // status update
        std::cout << "working on sp3: " << curr->sp3Fname << "\n";

       // perform loop
       performLoop(idx, eopData);

        // update index
        idx = idx + 1;

        // advance list
        curr = curr->next;
    }

};


