compile() {
   g++ -O -o test.x test.cpp tlelist.o tle.o eop.o sp3.o sp3list.o \
   truth.o pred.o residual.o residualList.o lageos1Model.o lageos2Model.o sp3FileList.o \
   SGP4/libsgp4.a SOFA/libsofa_c.a thalassa/libthalassa.a $1 $2 $3 $4 $5
}
spice=~/Documents/codes/fortran/SPICE/toolkit/lib/spicelib.a
sofa=~/Documents/codes/fortran/SOFA/src/libsofa.a
gccl=/usr/local/gfortran/lib/gcc/x86_64-apple-darwin16/6.3.0/libgcc.a
gfl=/usr/local/gfortran/lib/libgfortran.a
qml=/usr/local/gfortran/lib/libquadmath.a
make -C thalassa
make -C SGP4
g++ -c lageos1Model.cpp
g++ -c lageos2Model.cpp
g++ -c eop.cpp
g++ -c tle.cpp
g++ -c tlelist.cpp
g++ -c sp3.cpp
g++ -c sp3list.cpp
g++ -c truth.cpp
g++ -c pred.cpp
g++ -c residual.cpp
g++ -c residualList.cpp
g++ -c sp3FileList.cpp
compile $spice $sofa $gccl $gfl $qml
