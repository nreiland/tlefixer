##################
# script to download TLE ensemble according to given NORAD ID text list
# Contributor:
#           Ricardo Padilla - Aug. 1.2019 - Basic method of retrieving all TLE files for any given object
#           Di Wu           - Sep.13.2019 - Retrieving latest TLE file for any given object
#           Di Wu           - Jul. 1.2019 - Convert into function with input of objects number, time limit, and user info
#
# Usage: python3 Fetching_TLE_From_Spacetrack_NTN.py 8820 58110 58500 'spaceid' 'spacepassword'
##################

import requests
import os.path
import time
import datetime as t
import math
import sys

def mjd_to_jd(mjd):
    """
    Convert Modified Julian Day to Julian Day.
        
    Parameters
    ----------
    mjd : float
        Modified Julian Day
        
    Returns
    -------
    jd : float
        Julian Day
    
        
    """
    return mjd + 2400000.5
    
def jd_to_date(jd):
    """
    Convert Julian Day to date.
    
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    
    Parameters
    ----------
    jd : float
        Julian Day
        
    Returns
    -------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
        
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    
    day : float
        Day, may contain fractional part.
        
    Examples
    --------
    Convert Julian Day 2446113.75 to year, month, and day.
    
    >>> jd_to_date(2446113.75)
    (1985, 2, 17.25)
    
    """
    jd = jd + 0.5
    
    F, I = math.modf(jd)
    I = int(I)
    
    A = math.trunc((I - 1867216.25)/36524.25)
    
    if I > 2299160:
        B = I + 1 + A - math.trunc(A / 4.)
    else:
        B = I
        
    C = B + 1524
    
    D = math.trunc((C - 122.1) / 365.25)
    
    E = math.trunc(365.25 * D)
    
    G = math.trunc((C - E) / 30.6001)
    
    day = C - E + F - math.trunc(30.6001 * G)
    
    if G < 13.5:
        month = G - 1
    else:
        month = G - 13
        
    if month > 2.5:
        year = D - 4716
    else:
        year = D - 4715
        
    return year, month, day

def mjd_to_date(mjd):
    """
    Convert mjd to date time (year, month, day_and_fractional_day)
    
    Parameters
    ----------
    mjd :
        Modified Julian Date
    
    Returns
    -------
    year: int
    mont: int
    day_and_fractional_day: float
        day and the following fractional number, e.x. 18.921
    """
    
    return jd_to_date(mjd_to_jd(mjd))

def Fetching_TLE_From_Spacetrack_NTN(NORID_ID, MJD_lower_epoch, MJD_higher_epoch, fname, my_spacetrack_ID, my_spacetrack_password):
    save_path ='.'
    s = requests.Session()
    login_data = {'identity':my_spacetrack_ID,'password':my_spacetrack_password}
    s.post('https://www.space-track.org/ajaxauth/login', data=login_data)
    i=0
    x = NORID_ID  #Store the NORAD ID of interested satellites
    MJD1 = math.floor(MJD_lower_epoch) # always rounding to lower integer
    MJD2 = math.ceil(MJD_higher_epoch) # always rounding to upper integer
    # mjds process into strings
    date1_year,date1_month,date1_day = mjd_to_date(MJD1)
    date2_year,date2_month,date2_day = mjd_to_date(MJD2)
    date1_str = str(date1_year)+'-'+str(date1_month).zfill(2)+'-'+str(math.floor(date1_day)).zfill(2)
    date2_str = str(date2_year)+'-'+str(date2_month).zfill(2)+'-'+str(math.ceil(date2_day)).zfill(2)

    #Gathers Historical TLE data from SpaceTrack and saves into .txt files
    ################ Retrieving all TLE files
    #    URL=('https://www.space-track.org/basicspacedata/query/class/tle/format/tle/NORAD_CAT_ID/') # retrieve all historical tle
    #    URL='%s%s' %(URL,*x[i])
    ################ Retrieving latest TLE files only
    URL=('https://www.space-track.org/basicspacedata/query/class/tle/format/tle/EPOCH/'+date1_str+'--'+date2_str+'/NORAD_CAT_ID/') # retrieve only latest
    URL='%s%s' %(URL,str(x))
    URL = URL+'/orderby/TLE_LINE1 ASC/format/tle' # retrieve only latest
    print(URL)
    num=str(x)
    num=int(num)
    r = s.get(URL)
    h='Historical_TLE_'
    t='_MJD_'+str(MJD1)+'_to_'+str(MJD2)
    name='%s%05d%s' %(h,num,t)
    completeName = os.path.join(save_path, fname)
    with open(completeName,'w',newline='') as fp:
        for line in r.text:
         fp.write(line)
    i+=1
    if i % 20==0:
        time.sleep(60)
    if i==200:
        time.sleep(3600)
    if i==400:
        time.sleep(3600)
    fp.close()




#id = 'woodywu@email.arizona.edu'
#password = 'Deskjet3848woody'
#tle_objects_list = 8820
#MJD_l = 58110
#MJD_h = 58500
#Fetching_TLE_From_Spacetrack_NTN(tle_objects_list, MJD_l, MJD_h, id, password)
if __name__ == "__main__":
    print('NORID ',sys.argv[1])
    Fetching_TLE_From_Spacetrack_NTN(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), sys.argv[4], sys.argv[5], sys.argv[6])


    
