// fixerlib.h
// contains class declarations

#ifndef fixerlib
#define fixerlib

#include <string>
#include <vector>
#include <iostream>
#include <fstream> 

namespace fixer {

    // physical model structure
        typedef struct {
        double tstep;
        int insgrav;
        int isun;
        int imoon;
        int idrag;
        int iF107;
        int iSRP;
        int iephem;
        int gdeg;
        int gord;
        double tol;
        int imcoll;
        int eqs;
        double SCMass;
        double ADrag;
        double ASRP;
        double CD;
        double CR; 
    }physModel; 

    // physical constas
    #define dms2kms (1.0/10.0/1000.0)

    // pyshical models
    physModel lageos1Model();
    physModel lageos2Model();

    // eop hash table class
    class eop {

        private:

            std::vector<double> mjd;
            std::vector<double> pmx;
            std::vector<double> pmy;
            std::vector<double> UT1_UTC;
            std::vector<double> LOD;
            std::vector<double> dx00;
            std::vector<double> dy00;

        public:

            // constructor
            eop(const char *fin);

            // vector index from mjd
            int index(int mjdEpoch);

            // initialization with data file
            void initialize(std::ifstream& ifs);

            // functions to get private data from index
            double getMjd(int idx);
            double getPmx(int idx);
            double getPmy(int idx);
            double getUt1_Utc(int idx);
            double getLod(int idx);
            double getDx00(int idx);
            double getDy00(int idx);

            // print list
            void print();
   
    };

    // tle class
    class tle {

        private:

            // satellite name
            std::string name;

            // satellite state
            std::vector<double> coe0;
            std::vector<double> meanCOE0;  
            std::vector<double> rteme0;
            std::vector<double> vteme0;
            std::vector<double> rpef0;
            std::vector<double> vpef0;
            std::vector<double> ritrf0;
            std::vector<double> vitrf0;
            std::vector<double> rgcrf0;
            std::vector<double> vgcrf0;

            // satellite epoch
            double jd0, fd0, mjd0;
            int iy0, im0, id0;

        public:

            // initial mean elements
            std::string getName();
            void setName(std::string name);

            // constructor
            tle();

            // initialize with two line element
            void initialize(std::string name, char line1[130], char line2[130]);

            // print tle
            void print();

            // coordinate system transformations
            void teme2pef(eop eopData);
            void pef2itrf(eop eopData);
            void itrf2gcrf(eop eopData);

            // convert gcrf coordinates to orbital elements
            void gcrf2coe();

            // function to get mjd
            double getMjd0();

            // function to get orbital elements
            std::vector<double> getCoe0();

            // function to get position vector
            std::vector<double> getGcrf0();

            // function to get velocity vector
            std::vector<double> getVgcrf0();

    };

    // tle linked list class
    class tlelist {

        private:

            struct node {
                tle obj;
                int idx;
                struct node *next;
            };

            typedef node* nodePtr;

            nodePtr head;
            nodePtr curr;

            std::string name;

        public:

            // constructor
            tlelist(const char *fin, eop eopData, std::string namein);


            // populate with file
            void populate(std::ifstream& ifs, eop eopData);

            // add a node to list
            void addNode(std::string name, char line1[130], char line2[130]);

            // delete a node from list
            void deleteNode(tle oldobj);

            // go to head
            void goToHead();

            // go to next node
            void advance();

            // get current node
            tle* getCurr();

            // print node parameters
            void printList();


    };

    // sp3 class
    class sp3 {

        private:

            // observation name
            std::string name;

            // observations epoch
            double mjd0, jd0, fd0;
            int iy0, im0, id0;

            // velocity conversion factor between sp3 file and km/s
            double vcon;

            // initial states
            std::vector<double> coe0;
            std::vector<double> ritrf0;
            std::vector<double> vitrf0;
            std::vector<double> rgcrf0;
            std::vector<double> vgcrf0;

        public:

            std::string getName();
            void setName(std::string name);

            // constructor
            sp3();

            // set velocity conversion factor
            void setVcon(double vcon);

            // initialize with two line element
            void initialize(std::string name, const char *line1, const char *line2, const char *line3);

            // convert from itrf to gcrf
            void itrf2gcrf(eop *eopData);

            // convert gcrf coordinates to orbital elements
            void gcrf2coe();

            // print tle
            void print();

            // function to get mjd
            double getMjd0();

            // function to get orbital elements
            std::vector<double> getCoe0();

            // function to get position vector
            std::vector<double> getRgcrf0();

            // function to get velocity vector
            std::vector<double> getVgcrf0();

    };

    // sp3 linked list
    class sp3list {

        private:

            struct node {
                sp3 obj;
                int idx;
                struct node *next;
            };

            typedef node* nodePtr;

            nodePtr head;
            nodePtr curr;

            std::string name;

            int skips;

            double vcon;

            eop *eopData;

        public:

            // constructor
            sp3list(const char *fin, eop *eopData, std::string namein, int skips, double vcon);

            // function to add node
            void addNode(std::string name, const char *line1, const char *line2, const char *line3);

            // function to populate list
            void populate(std::ifstream& ifs);

            // function to find node closest to given date
            sp3* findFromMjd(double mjd);

            // go to head
            void goToHead();

            // go to next node
            void advance();

            // get current node
            sp3* getCurr();

            // print list
            void printList();


    };

    // truth data class
    class truth {

        private:

            // observation name
            std::string name;

            // observations epoch
            double mjd;

            // state
            std::vector<double> coe;
            std::vector<double> rgcrf;
            std::vector<double> vgcrf;

        public:

            // constructor
            truth();

            // get name
            std::string getName();

            // set name
            void setName(std::string name);

            // initialize with two line element
            void genFromSp3(sp3 *sp3State);

            // propagate
            void propToEpoch(double mjdf, physModel model);

            // print object
            void print();

            // function to get mjd
            double getMjd();

            // function to get orbital elements
            std::vector<double> getCoe();

            // function to get position vector
            std::vector<double> getRgcrf();

            // function to get velocity vector
            std::vector<double> getVgcrf();

    };

    // predicted data class
    class pred {

        private:

            // observation name
            std::string name;

            // observations epoch
            double mjd;

            // state
            std::vector<double> coe;
            std::vector<double> rgcrf;
            std::vector<double> vgcrf;

        public:

            // constructor
            pred();

            // get name
            std::string getName();

            // set name
            void setName(std::string name);

            // initialize with two line element
            void genFromTle(tle *tleState);

            // print object
            void print();

            // function to get mjd
            double getMjd();

            // function to get orbital elements
            std::vector<double> getCoe();

            // function to get position vector
            std::vector<double> getRgcrf();

            // function to get velocity vector
            std::vector<double> getVgcrf();

    };

    // residual data class
    class residual {

        private:

            // observation name
            std::string name;

            // observations epoch
            double mjd;

            // errors state
            std::vector<double> errCoe;
            std::vector<double> errRgcrf;
            std::vector<double> errVgcrf;

        public:

            // constructor
            residual();

            // get name
            std::string getName();

            // set name
            void setName(std::string name);

            // initialize with measurements
            void compute(pred predState, truth truthState);

            // print object
            void print();

            // function to get epoch
            double getMjd();

            // function to get orbital elements
            std::vector<double> getErrCoe();

            // function to get position vector
            std::vector<double> getErrRgcrf();

            // function to get velocity vector
            std::vector<double> getErrVgcrf();

    };

    // residual list class
    class residualList {

        private:

            struct node {
                residual obj;
                truth truthState;
                pred predState;
                int idx;
                struct node *next;
            };

            typedef node* nodePtr;

            nodePtr head;
            nodePtr curr;

            std::string name;

        public:

            // constructor
            residualList();

            // function to add node
            void addNode(truth truthState, pred predState);

            // function to populate list with sp3s and tles
            void populate(tlelist *tles, sp3list *sp3s, physModel model);

            // print list
            void printList();

            // write to file
            void writeToFile(const char *ftrue, const char *fpred, const char *fres);

    };

};

// class for sp3 file list -> should also download and save tles
class sp3FileList {

        private:

            struct node {
                double mjd0;
                double mjdf;
                std::string tleFname;
                std::string sp3Fname;
                struct node *next;
            };

            typedef node* nodePtr;

            nodePtr head;
            nodePtr curr;

            std::string name;

            int nID;

            int idx;

            std::string tleDir;
            std::string resDir;
            std::string STusername;
            std::string STpassword;
            std::string feop;

            // function to download tles
            void downloadTles(int mjd0, int mjdf, std::string sp3Fname);

            // function to read tle date ranges
            void dateRanges(const char *sp3Fname, double *mjd0, double *mjdf);

            // helper function to iterate
            void performLoop(int idx, fixer::eop eopData);

        public:

            // constructor
            sp3FileList();

            // function to set list norad id
            void setNoradId(int nID);

            // function to set tle data output dir
            void setTleDir(std::string tleDir);

            // funciton to set residual output dir
            void setResidualDir(std::string resDir);

            // function to set space track account details
            void setSpaceTrackInfo(std::string username, std::string password);

            // function to set eop file
            void setEopFile(std::string feop);

            // function to add node
            void addNode(const char *sp3Fname, const char *tleFname);

            // function to populate list with sp3s and tles
            void populate(const char *dirName);

            // function to uncompress
            void uncompress(const char *dirName);

            // function to generate residuals for list
            void genResiduals();

            // print list
            void printList();

    };


#endif