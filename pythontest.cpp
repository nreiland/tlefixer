#include <stdio.h>
#include "/Library/Frameworks/Python.framework/Versions/3.7/Headers/Python.h"
#include "/Library/Frameworks/Python.framework/Versions/3.7/Headers/import.h"

int main()
{

	Py_Initialize();

    PyObject* pmodule = PyImport_ImportModule("pyemb");


	PyRun_SimpleString("print('Hello World from Embedded Python!!!')");
	
	Py_Finalize();

	printf("\nPress any key to exit...\n");

	return 0;
}