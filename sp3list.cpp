//sp3list.cpp
#include<cstdlib>  
#include"fixerlib.h"
#include<string>
#include<iostream>
#include<fstream>

using namespace fixer;

sp3list::sp3list(const char *fin, eop *eopData, std::string namein, int skips, double vcon) {

    // locals
    int n, i;
    char line[200];
    char sval[10];
    const char *format;

    // head node
    head = NULL;
    curr = NULL;
    name = namein;

    // number of lines to skip when reading in sp3 states
    this->skips = skips;

    // set eop data
    this->eopData = eopData;

    // set velocity conversion factor
    this->vcon = vcon;

    // open input file
    std::ifstream input;
    input.open(fin, std::ifstream::in);

    // skip lines of input to bypass header of sp3 using while loop as in readtle header
    format = "%1s%*59c\n";
    input.getline(line, 200);
    sscanf(line, format, sval);

    // count lines
    n = 0;
    while (*sval != '*') {
        n = n + 1;
        input.getline(line, 200);
        sscanf(line, format, sval);
    }
    input.close();

    // reopen
    input.open(fin, std::ifstream::in);
    for (i = 0; i < n; i++) {
        input.getline(line, 200);
    }

    // populate
    sp3list::populate(input);
};

void sp3list::addNode(std::string name, const char *line1, const char *line2, const char *line3) {

    // define new TLE node
    nodePtr n = new node;
    n->next = NULL;
    n->obj.setVcon(vcon);
    n->obj.initialize(name, line1, line2, line3);

    if (head != NULL) {
        curr = head;
        while (curr->next != NULL) {
            curr = curr->next;
        }
        curr->next = n;
        curr->next->idx = curr->idx + 1;
        curr = curr->next;
    }
    else {
        head = n;
        curr = head;
        curr->idx = 0;
    }

};

void sp3list::populate(std::ifstream& ifs) {

    // local variables
    char line1[200];
    char line2[200];
    char line3[200];
    char dummy[200];

    // end of file reached
    if (ifs.eof()) {
        ifs.close();
        return;
    }

    // end of file not reached
    else {
        
        // read lines
        ifs.getline(line1,200);
        if (ifs.eof()) {
            ifs.close();
            return;
        }
        ifs.getline(line2,200);
        if (ifs.eof()) {
            ifs.close();
            return;
        }
        ifs.getline(line3,200);
        if (ifs.eof()) {
            ifs.close();
            return;
        }

        // construct sp3 name
        std::string s(line2);
        std::string lname(s, 2, 3); //starting at 2, with a length of 3

        // add node
        addNode(lname, line1, line2, line3);

        // add index to name
        curr->obj.setName(name + "_" + std::to_string(curr->idx));

        // transform satellite state into GCRF from ITRF
        curr->obj.itrf2gcrf(eopData);

        // transform gcrf to coe
        curr->obj.gcrf2coe();

        // skip lines
        if (skips > 0) {
            for (int i = 0; i < skips; i++) {
                ifs.getline(dummy, 200);
                if (ifs.eof()) {
                    ifs.close();
                    return;
                }
                ifs.getline(dummy, 200);
                if (ifs.eof()) {
                    ifs.close();
                    return;
                }
                ifs.getline(dummy, 200);
                if (ifs.eof()) {
                    ifs.close();
                    return;
                }
            }
        }

        // recurse
        sp3list::populate(ifs);

    }

};

void sp3list::printList() {

    if (head != NULL) {
        curr = head;
        while (curr != NULL) {
            curr->obj.print();
            curr = curr->next;
        }
        std::cout << "[EoF reached]\n";
        
    }
    else {
        std::cout << "list is empty\n";
    }

        
};

// function to find node closest to given date 
sp3* sp3list::findFromMjd(double mjd) {

    // locals
    nodePtr optimal;
    double mjdDiffOld;
    double mjdDiff;

    // go to head
    curr = head;

    // check if head is null
    if (curr == NULL) {
        std::cout << "Error, list is empty\n";
        return NULL;
    }

    // otherwise initialize using head
    else {

        // set current optimal
        optimal = curr;
        mjdDiffOld = mjd - optimal->obj.getMjd0();

        // check if first diff is after mjd
        if (mjdDiffOld < 0){
            std::cout << "Error, the first mjd from sp3 list is after the desired date\n";
            return NULL;
        }

        // update current
        curr = curr->next;
    }

    // iterate using while loop
    while (curr != NULL) {

        // calculate current mjd differece
        mjdDiff = mjd - curr->obj.getMjd0();

        // check if mjdDiff has become negative
        if (mjdDiff < 0) {
            break;
        }

        // check if next mjd is closer to optimal
        if (mjdDiff < mjdDiffOld) {

            // set new optimal
            optimal = curr;
            mjdDiffOld = mjd - optimal->obj.getMjd0();
        }

        // update current
        curr = curr->next;
    
    }

    // return the optimal sp3
    return &optimal->obj;

};

// go to head
void sp3list::goToHead() {
    curr = head;
};

// function to go to next node
void sp3list::advance() {
    curr = curr->next;
}

// get current node
sp3* sp3list::getCurr() {
    if (curr == NULL) {
        return NULL;
    }
    else {
        return &curr->obj;
    }
    
};