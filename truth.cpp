// truth.cpp

#include"fixerlib.h"
#include"./thalassa/thalassa.h"

using namespace fixer;

// constructor
truth::truth() {

    // default name
    name = "uninitialized";

    // epoch
    mjd = 0;

    // allocate orbital elements
    for (int i = 0; i < 6; i++) {
         coe.push_back(0);
    }

    // allocate Cartesian states
    for (int i = 0; i < 3; i++) {
        rgcrf.push_back(0);
        vgcrf.push_back(0);
    }

};

// function to retrieve name
std::string truth::getName() {
    return name;
};

// function to set name
void truth::setName(std::string name) {
    this->name = name;
};

// function to initialize truth state from TLE
void truth::genFromSp3(sp3 *sp3State) {

    // set name
    name = sp3State->getName();

    // set epoch
    mjd = sp3State->getMjd0();

    // set orbital elements
    coe = sp3State->getCoe0();

    // set position vector
    rgcrf = sp3State->getRgcrf0();

    // set velocity vector
    vgcrf = sp3State->getVgcrf0();  

}

// function to propagate state with Thalassa
void truth::propToEpoch(double mjdf, physModel model) {

    // locals
    thalassaInput in;

    // set thalassa input struct
    in.MJD0 = mjd;
    in.COE0[0] = coe[0];
    in.COE0[1] = coe[1]; 
    in.COE0[2] = coe[2]; 
    in.COE0[3] = coe[3]; 
    in.COE0[4] = coe[4];
    in.COE0[5] = coe[5];  
    in.tspan   = mjdf - mjd; 
    in.tstep   = model.tstep; 
    in.insgrav = model.insgrav; 
    in.isun    = model.isun; 
    in.imoon   = model.imoon; 
    in.idrag   = model.idrag; 
    in.iF107   = model.iF107; 
    in.iSRP    = model.iSRP; 
    in.iephem  = model.iephem; 
    in.gdeg    = model.iephem; 
    in.gord    = model.gord; 
    in.tol     = model.tol; 
    in.imcoll  = model.imcoll;
    in.eqs     = model.eqs; 
    in.SCMass  = model.SCMass; 
    in.ADrag   = model.ADrag; 
    in.ASRP    = model.ASRP; 
    in.CD      = model.CD; 
    in.CR      = model.CR; 

    // propagate
    thalassa(&in);

    // modify npts to account for difference in cpp and f90 indexing
    in.npts = in.npts - 1;

    // update current orbital elements
    coe[0] = in.orbs[in.npts * 7 + 1];
    coe[1] = in.orbs[in.npts * 7 + 2];
    coe[2] = in.orbs[in.npts * 7 + 3];
    coe[3] = in.orbs[in.npts * 7 + 4];
    coe[4] = in.orbs[in.npts * 7 + 5];
    coe[5] = in.orbs[in.npts * 7 + 6];

    // update current Cartesian state
    rgcrf[0] = in.cart[in.npts * 7 + 1];
    rgcrf[1] = in.cart[in.npts * 7 + 2];
    rgcrf[2] = in.cart[in.npts * 7 + 3];
    vgcrf[0] = in.cart[in.npts * 7 + 4];
    vgcrf[1] = in.cart[in.npts * 7 + 5];
    vgcrf[2] = in.cart[in.npts * 7 + 6];

    // update mjd
    mjd = mjdf;

    // free trajectories
    free(in.cart);
    free(in.orbs);
    
}

// function to get mjd
double truth::getMjd() {
    return mjd;
};

// function to get orbital elements
std::vector<double> truth::getCoe() {
    return coe;
};

// function to get position vector
std::vector<double> truth::getRgcrf() {
    return rgcrf;
};

// function to get velocity vector
std::vector<double> truth::getVgcrf() {
    return vgcrf;
};

// function to print object information
void truth::print() {

    std::cout << "\nConsidering truth state ";
    std::cout << name << ' ';

    std::cout << "\nThe Modified Julian is ";
    std::cout << mjd << "\n";

    std::cout << "\nThe Keplerian orbital elements are : ";
    for (int i=0; i < 6; i++) {
        std::cout << coe[i] << ' ';
    }

    std::cout << "\nThe GCRF position vector elements are : ";
    for (int i=0; i < rgcrf.size(); i++) {
        std::cout << rgcrf[i] << ' ';
    }

    std::cout << "\nThe GCRF velocity vector elements are : ";
    for (int i=0; i < vgcrf.size(); i++) {
        std::cout << vgcrf[i] << ' ';
    }

    std::cout << "\n";

};