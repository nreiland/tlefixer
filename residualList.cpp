//residualList.cpp
#include<cstdlib>  
#include"fixerlib.h"
#include<string>
#include<iostream>
#include<fstream>

using namespace fixer;

residualList::residualList() {

    // head node
    head = NULL;
    curr = NULL;
    name = "uninitialized";

};

void residualList::addNode(truth truthState, pred predState) {

    // define new residual node
    nodePtr n = new node;
    n->next = NULL;
    n->obj.setName(predState.getName() + "_" + "residual");
    n->truthState = truthState;
    n->predState = predState;
    n->obj.compute(predState, truthState);

    if (head != NULL) {
        curr = head;
        while (curr->next != NULL) {
            curr = curr->next;
        }
        curr->next = n;
        curr->next->idx = curr->idx + 1;
        curr = curr->next;
    }
    else {
        head = n;
        curr = head;
        curr->idx = 0;
    }

};

void residualList::populate(tlelist *tles, sp3list *sp3s, physModel model) {

    // local variables
    truth truthCurr;
    pred predCurr;
    tle *tleCurr;
    sp3 *sp3Curr;
    double mjd;
    int idx;

    // go to heads of lists
    tles->goToHead();
    sp3s->goToHead();

    // set initial index
    idx = 0;

    // iterate through tle list
    while (tles->getCurr() != NULL) {

        // status update
        std::cout << "working on tle: " << idx << "\n";

        // set current tle
        tleCurr = tles->getCurr();

        // generate predicted state
        predCurr.genFromTle(tleCurr);

        // get epoch mjd
        mjd = tleCurr->getMjd0();
        
        // find nearest sp3 state
        sp3Curr = sp3s->findFromMjd(mjd);

        // generate truth state
        truthCurr.genFromSp3(sp3Curr);

        // propagate truth state to epoch
        truthCurr.propToEpoch(mjd, model);

        // create new residual node
        addNode(truthCurr, predCurr);

        // add index to name
        curr->obj.setName(curr->obj.getName() + "_" + std::to_string(idx));

        // advance tle node
        tles->advance();

        // update index
        idx = idx + 1;
    }

};

// function to print list to terminal
void residualList::printList() {

    if (head != NULL) {
        curr = head;
        while (curr != NULL) {
            curr->obj.print();
            curr = curr->next;
        }        
    }
    else {
        std::cout << "list is empty\n";
    }
        
};

// function to write list to to file
void residualList::writeToFile(const char *ftrue, const char *fpred, const char *fres) {

    // locals
    double trueMjd;
    double predMjd;
    double resMjd;
    std::vector<double> trueRgcrf;
    std::vector<double> trueVgcrf;
    std::vector<double> predRgcrf;
    std::vector<double> predVgcrf;
    std::vector<double> predCoe;
    std::vector<double> trueCoe;
    std::vector<double> errCoe;
    std::vector<double> errRgcrf;
    std::vector<double> errVgcrf;

    // files
    FILE *trueOut;
    FILE *predOut;
    FILE *resOut;

    // Open true file
    trueOut = fopen(ftrue, "w");
    if (trueOut == NULL) {
        printf("Error opening file: %s!\n",ftrue);
        exit(-1);
    }

    // Open estimation file 
    predOut = fopen(fpred, "w");
    if (predOut == NULL) {
        printf("Error opening file: %s!\n",fpred);
        exit(-1);
    }

     // Open residuals file 
    resOut = fopen(fres, "w");
    if (resOut == NULL) {
        printf("Error opening file: %s!\n",fres);
        exit(-1);
    }

    // go to head
    if (head != NULL) {
        curr = head;
    }
    else {
        std::cout << "list is empty\n";
    }

    // iterate through list
    while (curr != NULL) {

        // get current values
        trueMjd   = curr->truthState.getMjd();
        predMjd   = curr->predState.getMjd();
        trueCoe   = curr->truthState.getCoe();
        predCoe   = curr->predState.getCoe();
        trueRgcrf = curr->truthState.getRgcrf();
        predRgcrf = curr->predState.getRgcrf();
        trueVgcrf = curr->truthState.getVgcrf();
        predVgcrf = curr->predState.getVgcrf();
        resMjd    = curr->obj.getMjd();
        errCoe    = curr->obj.getErrCoe();
        errRgcrf  = curr->obj.getErrRgcrf();
        errVgcrf  = curr->obj.getErrVgcrf();
        
        // write true line 
        fprintf(trueOut, "%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n",
                trueMjd, trueCoe[0], trueCoe[1], trueCoe[2], trueCoe[3],
                trueCoe[4], trueCoe[5], trueRgcrf[0], trueRgcrf[1], trueRgcrf[2],
                trueVgcrf[0], trueVgcrf[1], trueVgcrf[2]);

        // write pred line 
        fprintf(predOut, "%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n",
                predMjd, predCoe[0], predCoe[1], predCoe[2], predCoe[3],
                predCoe[4], predCoe[5], predRgcrf[0], predRgcrf[1], predRgcrf[2],
                predVgcrf[0], predVgcrf[1], predVgcrf[2]);

        // write pred line 
        fprintf(resOut, "%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n",
                resMjd, errCoe[0], errCoe[1], errCoe[2], errCoe[3],
                errCoe[4], errCoe[5], errRgcrf[0], errRgcrf[1], errRgcrf[2],
                errVgcrf[0], errVgcrf[1], errVgcrf[2]);

        // update current node
        curr = curr->next;
        
    }

    // close files
    fclose(trueOut);
    fclose(predOut);
    fclose(resOut);



}
