// tle.cpp

#include"fixerlib.h"
#include<string>
#include"SGP4/sgp4io.h"
#include"SGP4/sgp4unit.h"
#include"SOFA/sofa.h"
#include"./thalassa/thalassa.h"

using namespace fixer;

std::string tle::getName() {
    return name;
};

void tle::setName(std::string name) {
    this->name = name;
};

tle::tle() {
    
    name = "uninitialized";

    for (int i = 0; i < 6; i++) {
        meanCOE0.push_back(0);
        coe0.push_back(0);
    }

    for (int i = 0; i < 3; i++) {
        rteme0.push_back(0);
        vteme0.push_back(0);
        rpef0.push_back(0);
        vpef0.push_back(0);
        ritrf0.push_back(0);
        vitrf0.push_back(0);
        rgcrf0.push_back(0);
        vgcrf0.push_back(0);
    }
   
};

void tle::initialize(std::string name, char line1[130], char line2[130]) {

    // vallado outputs
    double startmfe;
    double stopmfe;
    double deltamin;
    elsetrec satrec;

    double r0[3];
    double v0[3];

    // initialize sgp4
    twoline2rv (
        line1, line2, 'c',  'e', 'i', wgs84,
        startmfe, stopmfe, deltamin, satrec
        );

    // call the propagator to get the initial state vector value
    sgp4(wgs84, satrec,  0.0, r0,  v0);

    // add mean elements to tle vector
    meanCOE0[0] = satrec.a;
    meanCOE0[1] = satrec.ecco;
    meanCOE0[2] = satrec.inclo;
    meanCOE0[3] = satrec.nodeo;
    meanCOE0[4] = satrec.argpo;
    meanCOE0[5] = satrec.mo;
        
    // add position and velocity in teme frame to tle vector
    for (int i = 0; i < 3; i++) {
        rteme0[i] = r0[i];
        vteme0[i] = v0[i];
    }

    // save date
    jd0 = satrec.jdsatepoch;

    // saved mjd0
    mjd0 = jd0 - 2400000.5;
}

void tle::print() {

    std::cout << "\nConsidering TLE ";
    std::cout << name << ' ';

    std::cout << "\nThe mean orbital elements are : ";
    for (int i=0; i < 6; i++) {
        std::cout << meanCOE0[i] << ' ';
    }

    std::cout << "\nThe TEME position vector elements are : ";
    for (int i=0; i < rteme0.size(); i++) {
        std::cout << rteme0[i] << ' ';
    }

    std::cout << "\nThe TEME velocity vector elements are : ";
    for (int i=0; i < vteme0.size(); i++) {
        std::cout << vteme0[i] << ' ';
    }

    std::cout << "\nThe PEF position vector elements are : ";
    for (int i=0; i < rpef0.size(); i++) {
        std::cout << rpef0[i] << ' ';
    }

    std::cout << "\nThe PEF velocity vector elements are : ";
    for (int i=0; i < vpef0.size(); i++) {
        std::cout << vpef0[i] << ' ';
    }

    std::cout << "\nThe ITRF position vector elements are : ";
    for (int i=0; i < ritrf0.size(); i++) {
        std::cout << ritrf0[i] << ' ';
    }

    std::cout << "\nThe ITRF velocity vector elements are : ";
    for (int i=0; i < vitrf0.size(); i++) {
        std::cout << vitrf0[i] << ' ';
    }

    std::cout << "\nThe GCRF position vector elements are : ";
    for (int i=0; i < rgcrf0.size(); i++) {
        std::cout << rgcrf0[i] << ' ';
    }

    std::cout << "\nThe GCRF velocity vector elements are : ";
    for (int i=0; i < vgcrf0.size(); i++) {
        std::cout << vgcrf0[i] << ' ';
    }

    std::cout << "\n";


};

void tle::teme2pef(eop eopData) {

    // function to rotate satellite state from TEME to PEF frame
    // polar motion is neglected in SGP4 and is typically neglected
    // in general perturbation theories. Thus the closest well-defined
    // frame for satellite states resulting from TLE propagation with
    // SGP4 is  the PEF frame. 

    // see chapter 3 page 233 of Vallado, "Fundamentals of Astrodynamics"

    // locals
    double deltat, utc, tai, tt;

    // TT and TAI from thalassa
    utc = jd0;
    iauJd2cal(utc, 0, &iy0, &im0, &id0, &fd0);
    iauDat(iy0, im0, id0, fd0, &deltat);
    tai = utc + deltat/DAYSEC;
    tt = tai + 32.184/DAYSEC;

    // get earth orientation data index
    int eopIdx;
    int epochMJD = jd0 - 2400000.5;
    eopIdx = eopData.index(epochMJD);   

    // split relevant time into components (mjd style)
    double UTCjd1 = 2400000.5;
    double UTCjd2 = utc - 2400000.5;
    double TTjd1 = 2400000.5;
    double TTjd2 = tt - 2400000.5;

    // get UT1
    double UT1jd1, UT1jd2;
    double dut1 = eopData.getUt1_Utc(eopIdx);   
    iauUtcut1(UTCjd1, UTCjd2, dut1, &UT1jd1, &UT1jd2);

    // get Greenwhich Mean Sidearal Time
    double gmst = iauGmst82(UT1jd1, UT1jd2);

    // rotate about R3
    double R[3][3];
    R[0][0] = 1;
    R[0][1] = 0;
    R[0][2] = 0;
    R[1][0] = 0;
    R[1][1] = 1;
    R[1][2] = 0;
    R[2][0] = 0;
    R[2][1] = 0;
    R[2][2] = 1;
    iauRz(gmst, R);

    // rotate position and velocity
    double Rpef[3], Vpef[3], Rteme[3], Vteme[3];
    Rteme[0] = rteme0[0];
    Rteme[1] = rteme0[1];
    Rteme[2] = rteme0[2];
    Vteme[0] = vteme0[0];
    Vteme[1] = vteme0[1];
    Vteme[2] = vteme0[2];
    iauRxp(R, Rteme, Rpef);
    iauRxp(R, Vteme, Vpef);

    // place in vectors
    rpef0[0] = Rpef[0];
    rpef0[1] = Rpef[1];
    rpef0[2] = Rpef[2];
    vpef0[0] = Vpef[0];
    vpef0[1] = Vpef[1];
    vpef0[2] = Vpef[2];

};

void tle::pef2itrf(eop eopData) {

    // function to transform position and velocity of a satellite in the
    // PEF frame to the ITRF. Note that following the transport theorem
    // the velocity in the body-fixed ITRF is reduced to account for the
    // contribution from the rotation of the frame.

    // see chapter 3 page 210 of Vallado, "Fundamentals of Astrodynamics"
    // also see chapter 3, page 208, fig 26
    // also see chapter 3, page 222

    // locals
    double deltat, utc, tai, tt;

    // TT and TAI from thalassa
    utc = jd0;
    iauJd2cal(utc, 0, &iy0, &im0, &id0, &fd0);
    iauDat(iy0, im0, id0, fd0, &deltat);
    tai = utc + deltat/DAYSEC;
    tt = tai + 32.184/DAYSEC;

    // get earth orientation data index
    int epochMJD = jd0 - 2400000.5;
    int eopIdx = eopData.index(epochMJD);

    // split relevant time into components (mjd style)
    double UTCjd1 = 2400000.5;
    double UTCjd2 = utc - 2400000.5;
    double TTjd1 = 2400000.5;
    double TTjd2 = tt - 2400000.5;

    // get UT1
    double UT1jd1, UT1jd2;
    double dut1 = eopData.getUt1_Utc(eopIdx);
    iauUtcut1(UTCjd1, UTCjd2, dut1, &UT1jd1, &UT1jd2);

    // get polar motion
    double Xp = eopData.getPmx(eopIdx) * DAS2R;
    double Yp = eopData.getPmy(eopIdx) * DAS2R;

    // length of day (convert ms to s)
    double LOD = eopData.getLod(eopIdx)/1000.0;

    // compute Earth rotation rate
    double OmegaEarth = 7.292115146706979e-5;
    double omegaE = OmegaEarth*(1.0 - (LOD/86400.0));

    // Polar motion matrix (TIRS->ITRS, IERS 2003) (W_T matrix)
    double W_T[3][3];
    iauPom00(Xp, Yp, iauSp00(TTjd1,TTjd2), W_T);

    // rotate position vector from PEF to ITRF
    double Rpef[3], Ritrf[3];
    Rpef[0] = rpef0[0];
    Rpef[1] = rpef0[1];
    Rpef[2] = rpef0[2];
    iauRxp(W_T, Rpef, Ritrf);

    // place in private vector
    ritrf0[0] = Ritrf[0];
    ritrf0[1] = Ritrf[1];
    ritrf0[2] = Ritrf[2];

    // get PEF velocity locallaly
    double Vpef[3];
    Vpef[0] = vpef0[0];
    Vpef[1] = vpef0[1];
    Vpef[2] = vpef0[2];

    // define angular velocity vector
    double omega[3];
    omega[0] = 0;
    omega[1] = 0;
    omega[2] = omegaE;

    // perform matrix operations
    double tempvec[3], tempvecb[3], tempmat[3][3];
    double Vitrf[3];
    iauPxp(omega, Rpef, tempvec);
    iauPmp(Vpef, tempvec, tempvecb);
    iauRxp(W_T, tempvecb, Vitrf);

    // place in vector
    vitrf0[0] = Vitrf[0];
    vitrf0[1] = Vitrf[1];
    vitrf0[2] = Vitrf[2];


};

void tle::itrf2gcrf(eop eopData) {

    // function to transform satellite position and velocity of a satellite in
    // the ITRF to the GCRP. The function follows the IAU standards, which are 
    // outlined in detail in SOFA documentation as well as in the cited Thesis.

    // see Reiland Thesis, 
    // "Assessing and Minimizing Collisions in Satellite Mega-Constellations",
    // page 41

    // locals
    double deltat, utc, tai, tt;

    // TT and TAI from thalassa
    utc = jd0;
    iauJd2cal(utc, 0, &iy0, &im0, &id0, &fd0);
    iauDat(iy0, im0, id0, fd0, &deltat);
    tai = utc + deltat/DAYSEC;
    tt = tai + 32.184/DAYSEC;

    // get earth orientation data index
    int epochMJD = jd0 - 2400000.5;
    int eopIdx = eopData.index(epochMJD);

    // split relevant time into components (mjd style)
    double UTCjd1 = 2400000.5;
    double UTCjd2 = utc - 2400000.5;
    double TTjd1 = 2400000.5;
    double TTjd2 = tt - 2400000.5;

    // get UT1
    double UT1jd1, UT1jd2;
    double dut1 = eopData.getUt1_Utc(eopIdx);
    iauUtcut1(UTCjd1, UTCjd2, dut1, &UT1jd1, &UT1jd2);

    // get polar motion
    double Xp = eopData.getPmx(eopIdx) * DAS2R;
    double Yp = eopData.getPmy(eopIdx) * DAS2R;

    // get CIP offsets wrt IAU 2000
    double dx00 = eopData.getDx00(eopIdx) * DMAS2R;
    double dy00 = eopData.getDy00(eopIdx) * DMAS2R;

    // length of day (convert ms to s)
    double LOD = eopData.getLod(eopIdx)/1000.0;

    // compute Earth rotation rate
    double OmegaEarth = 7.292115146706979e-5;
    double omegaE = OmegaEarth*(1.0 - (LOD/86400.0));

    // CIP and CIO, IAU 2000A (Represents the PN matrix in Vallado)
    double X, Y, S;
    iauXys00b(TTjd1, TTjd2, &X, &Y, &S);

    // add CIP corrections
    X = X + dx00;
    Y = Y + dy00;

    // GCRS to CIRS matrix (celestial to intermediate matrix [BPN]' = [N]'[P]'[B]')
    double BPN_T[3][3];
    iauC2ixys(X, Y, S, BPN_T);

    // Earth rotation angle
    double era = iauEra00(UT1jd1, UT1jd2);

    // rotation matrix about pole
    double R_T[3][3];
    R_T[0][0] = 1;
    R_T[0][1] = 0;
    R_T[0][2] = 0;
    R_T[1][0] = 0;
    R_T[1][1] = 1;
    R_T[1][2] = 0;
    R_T[2][0] = 0;
    R_T[2][1] = 0;
    R_T[2][2] = 1;
    iauRz(era, R_T);

    // Polar motion matrix (TIRS->ITRS, IERS 2003) (W_T matrix)
    double W_T[3][3];
    iauPom00(Xp, Yp, iauSp00(TTjd1,TTjd2), W_T);

    // multiply [R]',[BPN]',and [W]' 
    double tempmat1[3][3];
    double gcrf2itrf[3][3];
    iauRxr(R_T, BPN_T, tempmat1);
    iauRxr(W_T, tempmat1, gcrf2itrf);

    // get itrf to gcrf
    double itrf2gcrf[3][3];
    iauTr(gcrf2itrf, itrf2gcrf);
    
    // rotate position vector
    double Rgcrf[3], Ritrf[3];
    Ritrf[0] = ritrf0[0];
    Ritrf[1] = ritrf0[1];
    Ritrf[2] = ritrf0[2];
    iauRxp(itrf2gcrf, Ritrf, Rgcrf);

    // place in private vectors
    rgcrf0[0] = Rgcrf[0];
    rgcrf0[1] = Rgcrf[1];
    rgcrf0[2] = Rgcrf[2];

    // compute position and velocity vectors in the TIRS frame
    double W[3][3];
    double Rtirs[3], Vitrf[3], Vtirs[3];
    Vitrf[0] = vitrf0[0];
    Vitrf[1] = vitrf0[1];
    Vitrf[2] = vitrf0[2];
    iauTr(W_T, W);
    iauRxp(W,Ritrf,Rtirs);
    iauRxp(W,Vitrf,Vtirs);

    // define angular velocity vector
    double omega[3];
    omega[0] = 0;
    omega[1] = 0;
    omega[2] = omegaE;

    // perform matrix operations
    double tempvec[3], tempvecb[3], tempmat[3][3];
    double BPN[3][3], R[3][3], Vgcrf[3];
    iauTr(BPN_T, BPN);
    iauTr(R_T, R);
    iauPxp(omega, Rtirs, tempvec);
    iauPpp(Vtirs, tempvec, tempvecb);
    iauRxr(BPN, R, tempmat);
    iauRxp(tempmat, tempvecb, Vgcrf);

    // place in vector
    vgcrf0[0] = Vgcrf[0];
    vgcrf0[1] = Vgcrf[1];
    vgcrf0[2] = Vgcrf[2];

};

// function to convert rv in gcrf to Keplerian orbital elements
void tle::gcrf2coe() {

    // locals
    double mu, R[3], V[3], COE[6];

    // convert Cartesian state to Keplerian elements
    mu = MU;
    for (int i = 0; i < 3; i++) {
        R[i] = rgcrf0[i];
        V[i] = vgcrf0[i];
    }
    rv2coe(R, V, COE, mu);

    // save Keplerian orbital elements
    for (int i = 0; i < 6; i++) {
        coe0[i] = COE[i];
    }

    // convert to degrees
    coe0[2] = coe0[2]*r2d;
    coe0[3] = coe0[3]*r2d;
    coe0[4] = coe0[4]*r2d;
    coe0[5] = coe0[5]*r2d;

};

// function to get mjd
double tle::getMjd0() {
    return mjd0;
};

// function to get orbital elements
std::vector<double> tle::getCoe0() {
    return coe0;
};

// function to get position vector
std::vector<double> tle::getGcrf0() {
    return rgcrf0;
};

// function to get velocity vector
std::vector<double> tle::getVgcrf0() {
    return vgcrf0;
};

