// sp3.cpp

#include"fixerlib.h"
#include<string>
#include<iostream>
#include<iomanip>
#include"SGP4/sgp4io.h"
#include"SGP4/sgp4unit.h"
#include"SOFA/sofa.h"
#include"./thalassa/thalassa.h"

using namespace fixer;

// function to retrieve name
std::string sp3::getName() {
    return name;
};

// function to set name
void sp3::setName(std::string name) {
    this->name = name;
};

// constructor function
sp3::sp3() {
    
    // default name
    name = "uninitialized";

    // default velocity conversino factor
    vcon = 1;

    // allocate orbital elements
    for (int i = 0; i < 6; i++) {
         coe0.push_back(0);
    }

    // allocate Cartesian states
    for (int i = 0; i < 3; i++) {
        ritrf0.push_back(0);
        vitrf0.push_back(0);
        rgcrf0.push_back(0);
        vgcrf0.push_back(0);
    }
   
};

// function to set velocity modifier
void sp3::setVcon(double vcon) {
    this->vcon = vcon;
} 


// initialize with sp3 lines
void sp3::initialize(std::string name, const char *line1, const char *line2, const char *line3) {

    // string locals
    char iyears[50], imonths[50], idays[50], ihours[50], imins[50], secs[50];
    char vids[50], vids2[50], xcords[50], ycords[50], zcords[50], clocks[50];
    char xdots[50], ydots[50], zdots[50], dclocks[50];

    // number locals
    int iyear, imonth, iday, ihour, imin;
    double sec, xcord, ycord, zcord, clock;
    double xdot, ydot, zdot, dclock;
    double djm0, djm, time;

    // sscanf
    const char *format1, *format2, *format3;
    
    // epoch format
    format1 = "%*3c%4s%*c%2s%*c%2s%*c%2s%*c%2s%*c%11s";

    // position format
    format2 = "%*c%3s%14s%14s%14s%14s";

    // velocity format
    format3 = "%*4c%14s%14s%14s%14s";

    // scan
    sscanf(line1, format1, iyears, imonths, idays, ihours, imins, secs);
    sscanf(line2, format2, vids, xcords, ycords, zcords, clocks);
    sscanf(line3, format3, xdots, ydots, zdots, dclocks);

    // covert integers
    iyear  = std::stoi(iyears);
    imonth = std::stoi(imonths);
    iday   = std::stoi(idays);
    ihour  = std::stoi(ihours);
    imin   = std::stoi(imins);

    // convert doubles
    sec    = std::atof(secs); 
    xcord  = std::atof(xcords);
    ycord  = std::atof(ycords); 
    zcord  = std::atof(zcords);
    clock  = std::atof(clocks);
    xdot   = std::atof(xdots);
    ydot   = std::atof(ydots);
    zdot   = std::atof(zdots);
    dclock = std::atof(dclocks);

    // save position coordinates
    ritrf0[0] = xcord;
    ritrf0[1] = ycord;
    ritrf0[2] = zcord;

    // save velocity coordinates
    vitrf0[0] = xdot*vcon;
    vitrf0[1] = ydot*vcon;
    vitrf0[2] = zdot*vcon;

    // convert Calendar date to mjd and jd
    iauCal2jd(iyear, imonth, iday, &djm0, &djm);
    time = (60.0*(double)(60*ihour + imin) + sec)/DAYSEC;
    jd0  = djm0 + djm + time;
    mjd0 = jd0 - djm0;
};

// print sp3 data
void sp3::print() {

    std::cout << "\nConsidering sp3 state ";
    std::cout << name << ' ';

    std::cout << "\nThe Julian and Modified Julian dates are ";
    std::cout << jd0 << " / " << mjd0 << "\n";

    std::cout << "\nThe Keplerian orbital elements are : ";
    for (int i=0; i < 6; i++) {
        std::cout << coe0[i] << ' ';
    }

    std::cout << "\nThe ITRF position vector elements are : ";
    for (int i=0; i < ritrf0.size(); i++) {
        std::cout << ritrf0[i] << ' ';
    }

    std::cout << "\nThe ITRF velocity vector elements are : ";
    for (int i=0; i < vitrf0.size(); i++) {
        std::cout << vitrf0[i] << ' ';
    }

    std::cout << "\nThe GCRF position vector elements are : ";
    for (int i=0; i < rgcrf0.size(); i++) {
        std::cout << rgcrf0[i] << ' ';
    }

    std::cout << "\nThe GCRF velocity vector elements are : ";
    for (int i=0; i < vgcrf0.size(); i++) {
        std::cout << vgcrf0[i] << ' ';
    }

    std::cout << "\n";

};

void sp3::itrf2gcrf(eop *eopData) {

    // function to transform satellite position and velocity of a satellite in
    // the ITRF to the GCRP. The function follows the IAU standards, which are 
    // outlined in detail in SOFA documentation as well as in the cited Thesis.

    // see Reiland Thesis, 
    // "Assessing and Minimizing Collisions in Satellite Mega-Constellations",
    // page 41

    // locals
    double deltat, utc, tai, tt;

    // TT and TAI from thalassa
    utc = jd0;
    iauJd2cal(utc, 0, &iy0, &im0, &id0, &fd0);
    iauDat(iy0, im0, id0, fd0, &deltat);
    tai = utc + deltat/DAYSEC;
    tt = tai + 32.184/DAYSEC;

    // get earth orientation data index
    int epochMJD = jd0 - 2400000.5;
    int eopIdx = eopData->index(epochMJD);

    // split relevant time into components (mjd style)
    double UTCjd1 = 2400000.5;
    double UTCjd2 = utc - 2400000.5;
    double TTjd1 = 2400000.5;
    double TTjd2 = tt - 2400000.5;

    // get UT1
    double UT1jd1, UT1jd2;
    double dut1 = eopData->getUt1_Utc(eopIdx);
    iauUtcut1(UTCjd1, UTCjd2, dut1, &UT1jd1, &UT1jd2);

    // get polar motion
    double Xp = eopData->getPmx(eopIdx) * DAS2R;
    double Yp = eopData->getPmy(eopIdx) * DAS2R;

    // get CIP offsets wrt IAU 2000
    double dx00 = eopData->getDx00(eopIdx) * DMAS2R;
    double dy00 = eopData->getDy00(eopIdx) * DMAS2R;

    // length of day (convert ms to s)
    double LOD = eopData->getLod(eopIdx)/1000.0;

    // compute Earth rotation rate
    double OmegaEarth = 7.292115146706979e-5;
    double omegaE = OmegaEarth*(1.0 - (LOD/86400.0));

    // CIP and CIO, IAU 2000A (Represents the PN matrix in Vallado)
    double X, Y, S;
    iauXys00b(TTjd1, TTjd2, &X, &Y, &S);

    // add CIP corrections
    X = X + dx00;
    Y = Y + dy00;

    // GCRS to CIRS matrix (celestial to intermediate matrix [BPN]' = [N]'[P]'[B]')
    double BPN_T[3][3];
    iauC2ixys(X, Y, S, BPN_T);

    // Earth rotation angle
    double era = iauEra00(UT1jd1, UT1jd2);

    // rotation matrix about pole
    double R_T[3][3];
    R_T[0][0] = 1;
    R_T[0][1] = 0;
    R_T[0][2] = 0;
    R_T[1][0] = 0;
    R_T[1][1] = 1;
    R_T[1][2] = 0;
    R_T[2][0] = 0;
    R_T[2][1] = 0;
    R_T[2][2] = 1;
    iauRz(era, R_T);

    // Polar motion matrix (TIRS->ITRS, IERS 2003) (W_T matrix)
    double W_T[3][3];
    iauPom00(Xp, Yp, iauSp00(TTjd1,TTjd2), W_T);

    // multiply [R]',[BPN]',and [W]' 
    double tempmat1[3][3];
    double gcrf2itrf[3][3];
    iauRxr(R_T, BPN_T, tempmat1);
    iauRxr(W_T, tempmat1, gcrf2itrf);

    // get itrf to gcrf
    double itrf2gcrf[3][3];
    iauTr(gcrf2itrf, itrf2gcrf);
    
    // rotate position vector
    double Rgcrf[3], Ritrf[3];
    Ritrf[0] = ritrf0[0];
    Ritrf[1] = ritrf0[1];
    Ritrf[2] = ritrf0[2];
    iauRxp(itrf2gcrf, Ritrf, Rgcrf);

    // place in private vectors
    rgcrf0[0] = Rgcrf[0];
    rgcrf0[1] = Rgcrf[1];
    rgcrf0[2] = Rgcrf[2];

    // compute position and velocity vectors in the TIRS frame
    double W[3][3];
    double Rtirs[3], Vitrf[3], Vtirs[3];
    Vitrf[0] = vitrf0[0];
    Vitrf[1] = vitrf0[1];
    Vitrf[2] = vitrf0[2];
    iauTr(W_T, W);
    iauRxp(W,Ritrf,Rtirs);
    iauRxp(W,Vitrf,Vtirs);

    // define angular velocity vector
    double omega[3];
    omega[0] = 0;
    omega[1] = 0;
    omega[2] = omegaE;

    // perform matrix operations
    double tempvec[3], tempvecb[3], tempmat[3][3];
    double BPN[3][3], R[3][3], Vgcrf[3];
    iauTr(BPN_T, BPN);
    iauTr(R_T, R);
    iauPxp(omega, Rtirs, tempvec);
    iauPpp(Vtirs, tempvec, tempvecb);
    iauRxr(BPN, R, tempmat);
    iauRxp(tempmat, tempvecb, Vgcrf);

    // place in vector
    vgcrf0[0] = Vgcrf[0];
    vgcrf0[1] = Vgcrf[1];
    vgcrf0[2] = Vgcrf[2];

};

// function to convert rv in gcrf to Keplerian orbital elements
void sp3::gcrf2coe() {

    // locals
    double mu, R[3], V[3], COE[6];

    // convert Cartesian state to Keplerian elements
    mu = MU;
    for (int i = 0; i < 3; i++) {
        R[i] = rgcrf0[i];
        V[i] = vgcrf0[i];
    }
    rv2coe(R, V, COE, mu);

    // save Keplerian orbital elements
    for (int i = 0; i < 6; i++) {
        coe0[i] = COE[i];
    }

    // convert to degrees
    coe0[2] = coe0[2]*r2d;
    coe0[3] = coe0[3]*r2d;
    coe0[4] = coe0[4]*r2d;
    coe0[5] = coe0[5]*r2d;

};

// function to get mjd
double sp3::getMjd0() {
    return mjd0;
};

// function to get orbital elements
std::vector<double> sp3::getCoe0() {
    return coe0;
};

// function to get position vector
std::vector<double> sp3::getRgcrf0() {
    return rgcrf0;
};

// function to get velocity vector
std::vector<double> sp3::getVgcrf0() {
    return vgcrf0;
};